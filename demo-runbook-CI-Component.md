
# Run the CI/CD component & Catalog Demo

## Demo Prework

- [ ] Remove the Forking Relationship
- [ ] Run a Pipeline Manualy
- [ ] Check Status of project, everything should be green

## Demo Runbook

### Inclusion of a GitLab Maintained CI component

1. In the just now forked Project, Open the Web IDE.
1. Ad to the top of your [.gitlab-ci.yml](./.gitlab-ci.yml) the [components/secret-detection](https://gitlab.com/explore/catalog/components/secret-detection) with the latest version, by adding the code block under this list.
1. go in the Web IDE to the Demo Secrets File [secrets.txt](./secrets.txt) and delete in line 4 the string `#gitleaks:allow`.
1. Commit both changes to the Project and go to the Pipeline view.
1. Show that Secret Detection is running now. You can also show the finding is you want.

```yaml
include:
  - component: gitlab.com/components/secret-detection/secret-detection@~latest
```

### Creation of a straightforward CI Component

1. Create a new blank project GitLab Project with a Project Name `Debug-Jobber - YOUR-GITLAB-HANDLE` in your Demo name space on GitLab.com, make it **public** with a description, for example: `Testing CI Component Creation and Publication YOUR-GITLAB-HANDLE - Only for Dmeo Purposes`. This will be the Project in which we will create the Ci-Component.
1. Open the Project in the Web IDE. (Athe the moment Edit Drop Down, then Web IDE)
1. In the Repository of this Project create the file `debugg-jobber.yml` in the folder templates. You can do this by entering this string in the new file creation modal in the root of your repository: `templates/debugg-jobber.yml`
1. Copy the contents of **[this yaml file](./ci-component-debug-jobber/templates/debugg-jobber.yml)** into your newly created file.
1. Create another file in the root of the new project `.gitlab-ci.yml`.
1. Copy the contents of **[this CiYaml File](./ci-component-debug-jobber/.gitlab-ci.yml)** into it.
1. Open the Readme.md and copy the content of [this ReadMe file](./ci-component-debug-jobber/README.md) into it.
1. The Repository of your newly created Project should be looking like this now:

```text
├── templates/
│   └── debugg-jobber.yml
└── .gitlab-ci.yml
└── README.md
```

- [ ] Porject created and filled, commit it with a message.
- [ ] Showed the Pipeline and it is green.

### Publication of the created CI Component

1. Add the [Logo](./images/Logo_-_Debug_Jobber.png) to your component Project.
2. [Set the ci component project as a catalog resource](https://docs.gitlab.com/ee/ci/components/index.html#set-a-component-project-as-a-catalog-resource)
   1. Select Settings > General in your project and expand Visibility, project features, permissions.
   2. Turn on the CI/CD Catalog resource toggle.
3. Create a [Git Tag from the UI](https://docs.gitlab.com/ee/user/project/repository/tags/#from-the-ui) in your project using  [semantic versioning](https://docs.gitlab.com/ee/ci/components/#use-semantic-versioning), for example `1.0.0`.
4. Check that the Pipeline with the release job runs green.
5. Got to in your project to Deploy > Releases, to chek if your release is present there.
6. Check in the [CI/CD Catalog](https://docs.gitlab.com/ee/ci/components/index.html#set-a-component-project-as-a-catalog-resource) if your component is present.

### Integration of just created Component

1. In your fork of the [Demo-CICD-component-catalog](url) Project, open the Piepline Editor.
1. Show the [CI/CD Catalog Button](https://gitlab.com/explore/catalog) in the Eidtor.
1. Add under the other present coponent of your [.gitlab-ci.yml](./.gitlab-ci.yml) your freshly created component with several stages as Input with different versions, by copying the Code Block out of the Ci Component entry in the CiCd Catalog for your entry, example code block under this list.
1. Check your Pipeline and show that the Component has been added several times with dynamic job Names.

```yaml
include:
  - component: gitlab.com/PATH-TO-YOUR-PROJECT/ci-component-debug-jobber/debugg-jobber@0.0.2
]   inputs:
      stage: build
  - component: gitlab.com/PATH-TO-YOUR-PROJECT/ci-component-debug-jobber/debugg-jobber@SHA-OF-A-COMMIT
    inputs:
      stage: 'test'
  - component: gitlab.com/PATH-TO-YOUR-PROJECT/ci-component-debug-jobber/debugg-jobber@~latest
    inputs:
      stage: '.post'
```

### End your demo

- Jump after the Demo to the [Breakdown instructions](./demo_breakdown.md)

## Troubleshooting

If you find any errors in this Project, please open a Merge Request against it and assign me (@fsieverding).

### Support

[Please create an issue in the Original Project](https://gitlab.com/gitlab-learn-labs/webinars/cicd/demo-cicd-component-catalog/-/issues/new) and add mention me (@fsieverding) with the descritption of the error. Check here for general [Ci component Troubleshooting](https://docs.gitlab.com/ee/ci/components/#troubleshooting).
