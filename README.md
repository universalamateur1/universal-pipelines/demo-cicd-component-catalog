# CI/CD component & Catalog Demo Project

> DRI: @fsieverding CSM DACH/EMEA at GitLab
>
> Last Update: 2024.03.01

![Pipeline Bricks Banner](./images/Banner_-_component_Catalog_Demo.png "Pipeline Bricks Banner")

## Goal and Purpose

Easy Out of the box Demo for CS at GitLab to demonstrate the functionality of the GitLab Feature [CI/CD Catalog](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) and [CI/CD components](https://docs.gitlab.com/ee/ci/components/index.html).

## What you will show

1. The Inclusion of a GitLab Maintained CI component into a small Pipeline in this Project.
2. The Creation of a straightforward CI Component.
3. The Publication of the created CI Component into [the CI/CD Catalog on GitLab.com](https://gitlab.com/explore/catalog/)
4. The Integration of just created Component into the same Pipeline.

## Usage

1. Fork this Project into your own Demo GitLab.com namespace.
2. __Optional__ Watch [the Walkthrough Video](url)
3. Follow the [Runbook](./demo-runbook-CI-Component.md).
4. Follow the [break down steps](./demo_breakdown.md).

## Additional Resources

- [Public slides: Efficient DevSecOps workflows with reusable CI/CD components](https://go.gitlab.com/VYS051) by @dnsmichi
- [GitLab Internal Slides file](https://docs.google.com/presentation/d/1tQhEnQ-pM8pW7bTHXIGKiQRRk8AinSj6KJ7NAj1J6eE/) a good start for your own Slides
- [Recording Talk: Efficient DevSecOps workflows with reusable CI/CD components](https://go.gitlab.com/nCSzd2) by @dnsmichi
- [CI/CD Catalog BluePrint](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/)
- [CI/CD components documentaiton](https://docs.gitlab.com/ee/ci/components/)
- [CI/CD component examples](https://docs.gitlab.com/ee/ci/components/examples.html)
- [Development guide for GitLab official CI/CD components](https://docs.gitlab.com/ee/development/cicd/components.html)

## ToDo

- [x] Dynamic Job Names
- [x] Add Logo and Banner
- [x] Publish Demo project into a shared space
- [ ] Publish the Proto Ci Component into a shared space
- [ ] Ingest Ci Component into Demo Project via Git Submodules
- [ ] Optimize CI Component
